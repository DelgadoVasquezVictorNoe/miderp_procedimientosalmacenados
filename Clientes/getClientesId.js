db.system.js.save({
    _id: "getClientesId",
    value: function (ides) {
        
        var array_personas = db.Persona.find({}).toArray();
        var array_estatus = db.Catalogo.find({_id : ObjectId("58e57696e75770120c60bf09")}).toArray();
        var arre = []
        db.Cliente.find((ides.length == 0) ? {} : {_id : {$in: ides} }).forEach( function(cliente) { 
            var mapa = {}
            
            mapa.id = cliente._id;
            
            var nombre = "";
            for (i in array_personas){
                if(cliente.IDPersona.str == array_personas[i]._id.str){
                    nombre = array_personas[i].Nombre;
                    break;
                }
            }
            
            mapa.Nombre = nombre;
            
            mapa.TipoCliente = cliente.TipoCliente;
            
            mapa.IDPersona = cliente.IDPersona;
            
            mapa.RFC = cliente.RFC;
            
            mapa.Direcciones = cliente.Direcciones;
            
            mapa.MediosDeContacto = cliente.MediosDeContacto;
            
            mapa.PersonasContacto = cliente.PersonasContacto;
            
            mapa.Almacenes = cliente.Almacenes;
            
            mapa.Notificaciones = cliente.Notificaciones;
           
            mapa.EstatusID = cliente.Estatus;
            
            estatus = "";
            for (i in array_estatus[0].Valores){
                if(cliente.Estatus.str === array_estatus[0].Valores[i]._id.str){
                    estatus = array_estatus[0].Valores[i].Valor;
                    break;
                }
            }
            mapa.Estatus = estatus;
            
            mapa.FechaHora = cliente.FechaHora;
	    
     
            arre.push(mapa) 
        });

        return arre
    }
})