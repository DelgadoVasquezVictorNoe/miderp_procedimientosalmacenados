db.system.js.save({
    _id: "getPedidos",
    value: function (ids, clave, estatusPedido) {

        var estatus_array = db.getCollection('Catalogo').find({
            "Clave": clave
        }, {
            "Valores": 1,
            "_id": 0
        });

        var arre = [];
        var indice = 1;

        db.Pedido.find((ids.length == 0) ? ((estatusPedido === "") ? {} : {
                "Estatus": ObjectId(estatusPedido)
            }) :
            (((estatusPedido === "")) ? {
                _id: {
                    $in: ids
                }
            } : {
                _id: {
                    $in: ids
                },
                "Estatus": ObjectId(estatusPedido)
            })
        ).forEach(function (pedido) {

            var mapa = {};

            mapa.id = indice;

            mapa.Folio = pedido._id.str;

            mapa.UsuarioNombre = pedido.UsuarioNombre;

            mapa.ClienteNombre = pedido.ClienteNombre;

            mapa.FormaDePagoNombre = pedido.FormaDePagoNombre;

            mapa.MedioDePagoNombre = pedido.MedioDePagoNombre;

            mapa.FormaDeEnvioNombre = pedido.FormaDeEnvioNombre;

            mapa.Monto = pedido.Monto;

            var estatus = ""
            for (i in estatus_array[0].Valores) {
                if (estatus_array[0].Valores[i]._id.valueOf() == pedido.Estatus.valueOf()) {
                    estatus = estatus_array[0].Valores[i].Valor;
                    break;
                }
            }
            mapa.Estatus = estatus;

            var mes = pedido.FechaInicio.getMonth() + 1;
            var fecha = pedido.FechaInicio.getFullYear() + "/" + mes + "/" + pedido.FechaInicio.getDate();
            mapa.FechaInicio = fecha;

            if (pedido.FechaEntrega !== undefined) {
                var mes2 = pedido.FechaEntrega.getMonth() + 1;
                var fecha2 = pedido.FechaEntrega.getFullYear() + "/" + mes2 + "/" + pedido.FechaEntrega.getUTCDate();
                mapa.FechaEntrega = fecha2;
            }

            //Aqui se deben obtener los datos de la coleccion (Se ponen por defecto porque aun no se determina en que manera se van a manejar esos valores)

            var Cobro = (pedido.Cobro !== undefined) ? pedido.Cobro : false;
            var Facturacion = (pedido.Facturacion !== undefined) ? pedido.Facturacion : false;
            var Entrega = (pedido.Entrega !== undefined) ? pedido.Entrega : false;
            var Backorder = (pedido.Backorder !== undefined) ? pedido.Backorder : false;

            if (!Cobro && !Facturacion && !Entrega) {
                mapa.Cancelar = "SI";
            } else {
                mapa.Cancelar = "NO";
            }

            arre.push(mapa);

            indice++;
        });

        return arre;
    }
})
