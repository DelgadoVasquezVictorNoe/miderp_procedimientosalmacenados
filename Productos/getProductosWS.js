db.system.js.save({
    _id: "getProductosWS",
    value: function (ides) {
        var array_unidades = db.Unidad.find({}).toArray();
        var array_tipos = db.CatalogoNuevo.find({_id : ObjectId("5a31787ae7577043e4f1a9d5")}).toArray();
        var array_estatus = db.CatalogoNuevo.find({_id : ObjectId("5a317770e7577043e4f1a9d4")}).toArray();
        var arre = []
        db.Producto.find((ides.length == 0) ? {} : {_id : {$in: ides} }).forEach( function(producto) { 
            var mapa = {}
            
            mapa.id = producto._id;
            mapa.Nombre = producto.Nombre;
            
            mapa.Codigos = producto.Codigos; 
            
            tipo = "";
            for (i in array_tipos[0].Elementos){
                if(producto.Tipo.str === array_tipos[0].Elementos[i].SubClave.str){
                    tipo = array_tipos[0].Elementos[i].Valor;
                    break;
                }
            }
            mapa.Tipo = tipo;
            
            mapa.Imagenes = [];
            
            mapa.unidades = producto.Unidades;
            
            for (i in producto.Unidades.Valores){
                for (j in array_unidades){
                    if(producto.Unidades.Valores[i]._id.str === array_unidades[j]._id.str){
                        mapa.unidades.Valores[i].Abreviatura = array_unidades[j].Abreviatura;
                        mapa.unidades.Valores[i].ide = producto.Unidades.Valores[i]._id.str;
                        if(producto.Unidades.Valores[i]._id.str === producto.Unidades.Default.str){
                            mapa.unidades.AbreviaturaDefault = array_unidades[j].Abreviatura;
                        }
                        break;
                    }
                }
            }
            
            mapa.CodigoSat = producto.CodigoSat;
            
            mapa.VentaFraccionCompra = producto.VentaFraccionCompra;
            
            mapa.VentaFraccionVenta = producto.VentaFraccionVenta;
            
            mapa.Etiquetas = producto.Etiquetas;
            
            mapa.Almacenes = producto.Almacenes;
            
            estatus = "";
            for (i in array_estatus[0].Elementos){
                if(producto.Estatus.str === array_estatus[0].Elementos[i].SubClave.str){
                    estatus = array_estatus[0].Elementos[i].Valor;
                    break;
                }
            }
            mapa.Estatus = estatus;
            
            mapa.FechaHora = producto.FechaHora;
            
            arre.push(mapa) 
        });
        
        return arre
    }
})