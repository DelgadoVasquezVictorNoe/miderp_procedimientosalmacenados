db.system.js.save({
    _id: "getCotizacion",
    value: function (ids) {
        
        var estatus_array = db.getCollection('Catalogo').find({_id: ObjectId("59a05e85ce11104475013f72")},{"Valores":1,"_id":0});
        var arre = [];
      
        db.Cotizacion.find((ids.length == 0) ? {} : {_id: { $in: ids } }).forEach( function(cotizacion) { 
            var mapa = {};
            
            mapa.id = cotizacion._id;
            
            if(cotizacion.Pedido != undefined){
                mapa.Pedido = cotizacion.Pedido;
            }
            
            if(cotizacion.Usuario != undefined){
                mapa.Usuario = cotizacion.Usuario;
            }
            mapa.UsuarioNombre = cotizacion.UsuarioNombre;
            
            if(cotizacion.Cliente != undefined){
                mapa.Cliente= cotizacion.Cliente;
            }
            mapa.ClienteNombre = cotizacion.ClienteNombre;
            
            mapa.Nombre = cotizacion.Nombre;
            
            mapa.Telefono = cotizacion.Telefono;
            
            mapa.Correo = cotizacion.Correo;
            
            if(cotizacion.FormaDePago != undefined){
                mapa.FormaDePago= cotizacion.FormaDePago;
            }
            mapa.FormaDePagoNombre = cotizacion.FormaDePagoNombre;
            
            if(cotizacion.MedioDePago != undefined){
                mapa.MedioDePago= cotizacion.MedioDePago;
            }
            mapa.MedioDePagoNombre = cotizacion.MedioDePagoNombre;
            
            if(cotizacion.FormaDeEnvio != undefined){
                mapa.FormaDeEnvio= cotizacion.FormaDeEnvio;
            }
            mapa.FormaDeEnvioNombre = cotizacion.FormaDeEnvioNombre;
            
            mapa.Monto = cotizacion.Monto;
            
            var estatus = ""
            for(i in estatus_array[0].Valores){
                if(estatus_array[0].Valores[i]._id.valueOf() == cotizacion.Estatus.valueOf()){
                    estatus = estatus_array[0].Valores[i].Valor;
                    break;
                }
            }
            mapa.Estatus = estatus;
            
            mapa.FechaInicio = cotizacion.FechaInicio
            
            mapa.FechaVigencia = cotizacion.FechaVigencia
            
            mapa.FechaFinal = cotizacion.FechaFinal
            
            
            arre.push(mapa);
 
        });
        
        return arre;
    }
})
