db.system.js.save({
    _id: "getCotizaciones",
    value: function (ids) {
        
        var estatus_array = db.getCollection('Catalogo').find({_id: ObjectId("59a05e85ce11104475013f72")},{"Valores":1,"_id":0});
        
        //indice_objeto = 0;
        var arre = [];
        var indice = 1;
      
        db.Cotizacion.find((ids.length == 0) ? {} : {_id: { $in: ids } }).forEach( function(cotizacion) { 
            var mapa = {};
            
            
            
            mapa.id = indice;
            
            mapa.Folio = cotizacion._id.str;
            
            mapa.Folio = cotizacion._id.str;
            
            mapa.UsuarioNombre = cotizacion.UsuarioNombre;
            
            mapa.ClienteNombre = cotizacion.ClienteNombre;
            
            mapa.FormaDePagoNombre = cotizacion.FormaDePagoNombre;
            
            mapa.MedioDePagoNombre = cotizacion.MedioDePagoNombre;
            
            mapa.FormaDeEnvioNombre = cotizacion.FormaDeEnvioNombre;
            
            mapa.Monto = cotizacion.Monto;
            
            var estatus = ""
            for(i in estatus_array[0].Valores){
                if(estatus_array[0].Valores[i]._id.valueOf() == cotizacion.Estatus.valueOf()){
                    estatus = estatus_array[0].Valores[i].Valor;
                    break;
                }
            }
            mapa.Estatus = estatus;
             
            var mes = cotizacion.FechaInicio.getMonth() + 1;            
            var fecha = cotizacion.FechaInicio.getFullYear()  + "/" + mes + "/" + cotizacion.FechaInicio.getDate();
            mapa.FechaInicio = fecha;
            
            if(cotizacion.FechaVigencia !== undefined ){
                if (cotizacion.FechaVigencia.getFullYear() == 1) {
                    mapa.FechaVigencia = fecha;
                } else {
                    var mes2 = cotizacion.FechaVigencia.getMonth() + 1;            
                    var fecha2 = cotizacion.FechaVigencia.getFullYear()  + "/" + mes2 + "/" + cotizacion.FechaVigencia.getUTCDate();
                    mapa.FechaVigencia = fecha2;
                }
            }
            
            
            arre.push(mapa);
            
            indice ++;
        });
        
        return arre;
    }
})